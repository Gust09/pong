package me.gust09.pong;

import java.awt.*;
import java.util.Random;

/**
 * Created by fabio_000 on 14/01/2017.
 */
public class Ball {

    private int x, y, width, height;
    private int diameter = 20;
    private int radius = diameter / 2;

    private int speedX = 7;
    private int speedY = 15;

    public Ball() {
        x = 50;
        y = 50;
    }

    public void update() {
        if (x + speedX <= 0) {
            // Player 2 scores

            Main.game.p2.addPoint();
            resetBall();

        }
        if (x + speedX >= Main.renderer.getWidth()) {
            // Player 1 scores

            Main.game.p1.addPoint();
            resetBall();

        }
        if (y + speedY <= 0) {
            speedY = speedY - 2 * speedY;
        }
        if (y + speedY >= Main.renderer.getHeight()) {
            speedY = speedY - 2 * speedY;
        }
        if ((x + speedX + radius <= Main.game.p1.getX() + Main.game.p1.getWidth())) {
            if (y + speedY + radius >= Main.game.p1.getY() && y + speedY <= Main.game.p1.getY() + Main.game.p1.getHeight()) {
                speedX = speedX - 2 * speedX;
                addEffect(1);
            }
        }
        if ((x + speedX + radius >= Main.game.p2.getX())) {
            if (y + speedY + radius >= Main.game.p2.getY() && y + speedY <= Main.game.p2.getY() + Main.game.p2.getHeight()) {
                speedX = speedX - 2 * speedX;
                addEffect(2);
            }
        }
        x += speedX;
        y += speedY;
    }

    private void addEffect(int p) {
        int hit;
        if (p == 1) {
            hit = y - Main.game.p1.getY() - Main.game.p1.getHeight() / 2;
            speedY += hit / 10;
        } else {
            hit = y - Main.game.p2.getY() - Main.game.p2.getHeight() / 2;
            speedY += hit / 10;
        }
        System.out.println(hit);
    }

    public void render(Graphics g) {
        g.setColor(Color.YELLOW);
        g.fillOval(x, y, diameter, diameter);
    }

    public void resetBall() {
        Random r = new Random();
        int chance = r.nextInt(100);

        x = Main.game.width / 2;
        y = Main.game.height / 2;
        speedY = 0;

        if (chance > 50) {
            speedX = 7;
        } else {
            speedX = -7;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
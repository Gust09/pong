package me.gust09.pong;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Main implements ActionListener, KeyListener {

    public static Main game;

    public static JFrame window;
    public static Renderer renderer;

    public int width = 800;
    public int height = 600;

    Pad p1;
    Pad p2;

    Ball ball;

    public boolean bot = false;

    public boolean w = false, s = false, up = false, down = false;

    public Main() {
        Timer timer = new Timer(20, this);

        window = new JFrame("Pong");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        renderer = new Renderer();

        window.add(renderer);
        window.pack();
        window.setSize(width + 7, height + 30);
        window.setVisible(true);

        window.addKeyListener(this);

        start();

        timer.start();

    }

    public void start() {
        p1 = new Pad(1);
        p2 = new Pad(2);

        ball = new Ball();

        if (bot)
            p2.setSpeed(p2.getSpeed() - 2);
    }

    public void update() {
        if (w) p1.moveUp();
        if (s) p1.moveDown();
        if (up) p2.moveUp();
        if (down) p2.moveDown();


        if (bot) {

            if (ball.getY() > p2.getY() + (p2.getHeight() / 2))
                p2.moveDown();
            else if (ball.getY() < p2.getY() + (p2.getHeight() / 2))
                p2.moveUp();


        }


        ball.update();
    }

    public void render(Graphics g) {
        g.setFont(new Font("Consolas", 0, 32));
        FontMetrics fontMetrics = g.getFontMetrics();

        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);

        g.setColor(Color.green);
        g.drawLine(renderer.getWidth() / 2, 0, renderer.getWidth() / 2, height);
        g.setColor(Color.BLACK);
        g.fillRect(renderer.getWidth()/2 - 100, 0, 200, 50);
        g.setColor(Color.GREEN);
        g.drawRect(renderer.getWidth()/2 - 100, 0, 200, 50);
        g.setFont(new Font("Consolas", 0, 12));
        fontMetrics = g.getFontMetrics();
        g.drawString("by Gust09", renderer.getWidth()/2 - fontMetrics.stringWidth("by Gust09")/2, 40);
        g.setFont(new Font("Consolas", 0, 32));
        fontMetrics = g.getFontMetrics();
        g.drawString("PONG", renderer.getWidth()/2 - fontMetrics.stringWidth("1234")/2, 25);
        g.drawString(p1.getScore() + "", 100, 50);
        g.drawString(p2.getScore() + "", renderer.getWidth() - fontMetrics.stringWidth(p2.getScore()+"")-100, 50);


        p1.render(g);
        p2.render(g);

        ball.render(g);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        update();
        renderer.repaint();
    }

    public static void main(String[] args) {
        game = new Main();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                w = true;
                break;
            case KeyEvent.VK_S:
                s = true;
                break;
            case KeyEvent.VK_UP:
                up = true;
                break;
            case KeyEvent.VK_DOWN:
                down = true;
                break;
            default:
                return;
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                w = false;
                break;
            case KeyEvent.VK_S:
                s = false;
                break;
            case KeyEvent.VK_UP:
                up = false;
                break;
            case KeyEvent.VK_DOWN:
                down = false;
                break;
            default:
                return;
        }
    }
}

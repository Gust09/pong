package me.gust09.pong;

import java.awt.*;

/**
 * Created by fabio_000 on 14/01/2017.
 */
public class Pad {

    private int padNumber;
    private int x, y, width = 5, height = 150;

    private int speed = 15;

    private int score;

    public Pad(int padNumber) {
        this.padNumber = padNumber;

        if (padNumber == 1) {
            this.x = 10;
        } else {
            this.x = Main.renderer.getWidth() - width - 10;
        }

        this.y = Main.renderer.getHeight() / 2 - (height / 2);
        this.score = 0;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void render(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(x, y, width, height);
    }

    public void moveUp() {
        if (y + speed > speed) {
            y -= speed;
        } else {
            y = 0;
        }
    }

    public void moveDown() {
        if (y + height + speed < Main.renderer.getHeight() + speed) {
            y += speed;
        } else {
            y = Main.renderer.getHeight() - height;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void addPoint() {
        score++;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}

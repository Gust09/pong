package me.gust09.pong;

import javax.swing.*;
import java.awt.*;

/**
 * Created by fabio_000 on 14/01/2017.
 */
public class Renderer extends JPanel {

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Main.game.render(g);
    }

}
